var express = require('express')
var cors = require('cors')
var app = express()
const axios = require('axios')
const port = 3000

app.use(cors())

app.get('/api/qoute', async (req, res) => {
  try {
    const response = await axios.get('http://34.101.207.75:8000/api/quote')
    res.status(200).json(response.data)
  } catch (err) {
    res.status(500).json({ message: err })
  }
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
